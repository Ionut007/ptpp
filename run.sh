rm sequential.jar && \
mvn clean compile assembly:single && \
cp ./target/sum-1.0-SNAPSHOT-jar-with-dependencies.jar sequential.jar

for i in {0..7}; do
  echo Starting test $i;
  java -jar sequential.jar $i;
  echo Finished test $i;
  echo;
done
