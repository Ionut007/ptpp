package com.lungu.polynomial;


import java.util.Objects;

public class DeadPillMonom extends Monom {
    public DeadPillMonom(double coefficient, long exponential) {
        super(coefficient, exponential);
    }
}
