package com.lungu.polynomial;


import java.util.Objects;

public class Monom implements Comparable<Monom> {
    private double coefficient;
    private long exponential;

    public Monom(double coefficient, long exponential) {
        this.coefficient = coefficient;
        this.exponential = exponential;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public long getExponential() {
        return exponential;
    }

    public void setExponential(long exponential) {
        this.exponential = exponential;
    }

    public boolean isRedundant() {
        return exponential == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Monom monom = (Monom) o;
        return exponential == monom.exponential;
    }

    @Override
    public int hashCode() {
        return Objects.hash(exponential);
    }

    @Override
    public String toString() {
        return "Monom{" +
                "coefficient=" + coefficient +
                ", exponential=" + exponential +
                '}';
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    @Override
    public int compareTo(Monom o) {
        return Long.compare(this.getExponential(), o.getExponential());
    }
}
