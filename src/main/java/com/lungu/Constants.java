package com.lungu;

import com.lungu.generator.TestCase;

import java.io.File;
import java.util.Arrays;

public class Constants {
    private final static String PATH = "./testcases/";
    private final static String FOLDER_NAME_FORMAT = "test-case-%d/";
    private final static String FILE_NAME_FORMAT = "Polynomial-%d.txt";

//    private final static TestCase[] TEST_CASES = new TestCase[] {
//            new TestCase(String.format(FOLDER_NAME_FORMAT, 0),   2,   100),
//            new TestCase(String.format(FOLDER_NAME_FORMAT, 1), 30, 3000),
//            new TestCase(String.format(FOLDER_NAME_FORMAT, 2),  3, 30000)
//    };

    private final static TestCase[] TEST_CASES = new TestCase[] {
            new TestCase(String.format(FOLDER_NAME_FORMAT, 0),   2,   100),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 1), 4, 300),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 2),  7, 1000),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 3),   11,   4000),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 4), 15, 7000),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 5),  18, 9000),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 6), 23, 10000),
            new TestCase(String.format(FOLDER_NAME_FORMAT, 7),  25, 20000)
    };

    public static String getPath() {
        return PATH;
    }

    public static String getFolderNameFormat() {
        return FOLDER_NAME_FORMAT;
    }

    public static String getFileNameFormat() {
        return FILE_NAME_FORMAT;
    }

    public static TestCase[] getTestCases() {
        return TEST_CASES;
    }

    public static File getTestCaseFile(String testCaseName, long polynomialNo) throws Exception {
        String localPath = Constants.getPath() + testCaseName;
        File directory = new File(localPath);
        directory.mkdirs();

        File file = new File(localPath + String.format(FILE_NAME_FORMAT, polynomialNo));
        file.createNewFile();
        return file;
    }
}
