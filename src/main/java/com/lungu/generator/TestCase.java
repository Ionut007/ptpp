package com.lungu.generator;

public class TestCase {
    private String fileName;
    private long noOfPolynomials;
    private long polynomialSize;

    public TestCase(String fileName, long noOfPolynomials, long polynomialSize) {
        this.fileName = fileName;
        this.noOfPolynomials = noOfPolynomials;
        this.polynomialSize = polynomialSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getNoOfPolynomials() {
        return noOfPolynomials;
    }

    public void setNoOfPolynomials(long noOfPolynomials) {
        this.noOfPolynomials = noOfPolynomials;
    }

    public long getPolynomialSize() {
        return polynomialSize;
    }

    public void setPolynomialSize(long polynomialSize) {
        this.polynomialSize = polynomialSize;
    }
}
