package com.lungu.generator;

import com.lungu.Constants;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Random;

public class TestCaseGenerator {
    private Random random;

    public TestCaseGenerator() {
    }

    public void generate(TestCase testCase) throws Exception {
        for(long polynomialIndex = 0; polynomialIndex < testCase.getNoOfPolynomials(); polynomialIndex++) {
            this.random = new Random();
            File file = Constants.getTestCaseFile(testCase.getFileName(), polynomialIndex);
            try(PrintWriter printWriter = new PrintWriter(new FileOutputStream(file))) {
                // generate poly data
                for(int i = 0; i < testCase.getPolynomialSize(); i++) {
                    printWriter.println(String.format("%.6f", nextDouble()) + " " + nextInt());
                }
                printWriter.flush();
            }
        }
    }

    private int nextInt() {
        int range = 110000;

        return random.nextInt(range * 2) - (range);
    }

    private double nextDouble() {
        long rangeMin = -3000; // Integer.MIN_VALUE;
        long rangeMax = 3000; // Integer.MAX_VALUE;
        return rangeMin + (rangeMax - rangeMin) * random.nextDouble();
    }
}
