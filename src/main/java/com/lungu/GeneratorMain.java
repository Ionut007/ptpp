package com.lungu;

import com.lungu.generator.TestCase;
import com.lungu.generator.TestCaseGenerator;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class GeneratorMain {
    public static void main(String[] args) throws Exception {
        File baseFolder = new File(Constants.getPath());
        FileUtils.deleteDirectory(baseFolder);
        if(!baseFolder.mkdir()) {
            System.err.println("Can't create base folder for test cases!");
        }

        System.out.println("Starting generating test cases...");
        TestCaseGenerator generator = new TestCaseGenerator();
        for(TestCase testCase : Constants.getTestCases()) {
            System.out.println("Test case: " + testCase.getFileName());
            generator.generate(testCase);
        }

        System.out.println("Generation finished!");
    }
}
