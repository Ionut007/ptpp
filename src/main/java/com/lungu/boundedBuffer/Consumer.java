package com.lungu.boundedBuffer;

import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.polynomial.DeadPillMonom;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;

import java.util.Queue;

public class Consumer extends Thread {
    private final Queue<Monom> buffer;

    public FullySynchronizedLinkedList getLinkedList() {
        return linkedList;
    }

    private final FullySynchronizedLinkedList linkedList;

    public Consumer(FullySynchronizedLinkedList linkedList, Queue<Monom> buffer) {
        this.buffer = buffer;
        this.linkedList = linkedList;
    }

    @Override
    public void run() {
        while(!(buffer.peek() instanceof DeadPillMonom)) {
            synchronized (buffer) {
                while (buffer.size() <= 0) {
                    try {
                        buffer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if(!(buffer.peek() instanceof DeadPillMonom)) {
                    linkedList.add(buffer.remove());
                    buffer.notifyAll();
                }
            }
        }
    }
}
