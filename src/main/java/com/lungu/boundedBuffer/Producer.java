package com.lungu.boundedBuffer;

import com.lungu.polynomial.DeadPillMonom;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;

import java.util.Queue;

public class Producer extends Thread {
    private final Reader reader;
    private final Queue<Monom> buffer;

    public Producer(Reader reader, Queue<Monom> buffer) {
        this.reader = reader;
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while(true) {
            try {
                if (!reader.hasNext()) {
                    // add pill
                    synchronized (buffer) {
                        buffer.add(new DeadPillMonom(0, 0));
                        buffer.notifyAll();
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            synchronized (buffer) {
                int maxSize = Integer.MAX_VALUE;
                while (buffer.size() >= maxSize) {
                    System.out.println(maxSize);
                    try {
                        buffer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    buffer.add(new Monom(reader.readDouble(), reader.readInt()));
                    buffer.notifyAll();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
