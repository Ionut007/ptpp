package com.lungu.boundedBuffer;

import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.polynomial.DeadPillMonom;
import com.lungu.polynomial.Monom;

import java.util.List;

public class SimpleMerger extends Thread {
    private final List<Monom> buffer;
    private final FullySynchronizedLinkedList linkedList;
    private final int start;
    private final int stop;

    public SimpleMerger(List<Monom> buffer, FullySynchronizedLinkedList linkedList, int start, int stop) {
        this.buffer = buffer;
        this.linkedList = linkedList;
        this.start = start;
        this.stop = stop;
    }

    @Override
    public void run() {
        for(int i = start; i < stop; i++) {
            Monom m = buffer.get(i);
            linkedList.add(m);
        }
    }
}
