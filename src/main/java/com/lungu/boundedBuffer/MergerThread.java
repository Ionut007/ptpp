package com.lungu.boundedBuffer;

import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.list.UnSynchronizedLinkedList;
import com.lungu.polynomial.Monom;

import java.util.List;

public class MergerThread extends Thread {
    private final UnSynchronizedLinkedList first;
    private final UnSynchronizedLinkedList second;

    public UnSynchronizedLinkedList getResult() {
        return result;
    }

    private UnSynchronizedLinkedList result;

    public MergerThread(UnSynchronizedLinkedList first, UnSynchronizedLinkedList second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public void run() {
        this.result = first.merge(second);
    }
}
