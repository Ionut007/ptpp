package com.lungu.boundedBuffer;

import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.polynomial.DeadPillMonom;
import com.lungu.polynomial.Monom;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MergerConsumer extends Thread {

    private final List<FullySynchronizedLinkedList> linkedLists;

    public FullySynchronizedLinkedList getResult() {
        return result;
    }

    private final FullySynchronizedLinkedList result;

    public MergerConsumer(List<FullySynchronizedLinkedList> linkedLists) {
        this.linkedLists = new ArrayList<>(linkedLists);
        this.result = this.linkedLists.remove(0);

    }

    @Override
    public void run() {
        for(FullySynchronizedLinkedList linkedList : linkedLists) {
            for(Monom monom : linkedList) {
                result.add(monom);
            }
        }
    }
}
