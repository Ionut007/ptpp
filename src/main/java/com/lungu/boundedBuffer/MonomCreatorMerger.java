package com.lungu.boundedBuffer;

import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.list.UnSynchronizedLinkedList;
import com.lungu.polynomial.Monom;

import java.util.List;

public class MonomCreatorMerger extends Thread {
    private final List<Monom> buffer;
    private final UnSynchronizedLinkedList linkedList;
    private final int start;
    private final int stop;

    public MonomCreatorMerger(List<Monom> buffer, UnSynchronizedLinkedList linkedList, int start, int stop) {
        this.buffer = buffer;
        this.linkedList = linkedList;
        this.start = start;
        this.stop = stop;
    }

    public UnSynchronizedLinkedList getLinkedList() {
        return linkedList;
    }

    @Override
    public void run() {
        for(int i = start; i < stop; i++) {
            Monom m = buffer.get(i);
            linkedList.add(m);
        }
    }
}
