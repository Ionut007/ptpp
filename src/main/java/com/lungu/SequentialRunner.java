package com.lungu;

import com.lungu.generator.TestCase;
import com.lungu.list.UnSynchronizedLinkedList;
import com.lungu.polynomial.Monom;
import com.lungu.readers.BufferedMonomReader;
import com.lungu.readers.ScannerReader;
import com.lungu.readers.Reader;
import com.lungu.readers.ResultWriter;

public class SequentialRunner implements TestCaseRunner {
    @Override
    public void run(TestCase testCase) throws Exception {
        UnSynchronizedLinkedList linkedList = new UnSynchronizedLinkedList();
        Reader reader = new ScannerReader(testCase);

        TimeLogger.getInstance().trace(testCase, "read-finished", false);

        while(reader.hasNext()) {
            double coefficient = reader.readDouble();
            int exponent = reader.readInt();
            linkedList.add(new Monom(coefficient, exponent));
        }

        TimeLogger.getInstance().trace(testCase, "algorithm-finished", true);

        ResultWriter writer = new ResultWriter("seq", testCase.getFileName());
        for(Monom monom : linkedList) {
            writer.write(monom);
        }
        writer.close();
        TimeLogger.getInstance().trace(testCase, "write-finished", false);
    }
}
