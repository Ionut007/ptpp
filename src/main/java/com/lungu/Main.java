package com.lungu;

import com.lungu.list.UnSynchronizedLinkedList;
import com.lungu.polynomial.Monom;
import com.lungu.runners.BarrierConsumerParallelRunner;
import com.lungu.runners.MergerParallelRunner;
import com.lungu.runners.SimpleMergerParallelRunner;

public class Main {
    public static void main(String[] args) throws Exception {
        int testCaseIndex = 0;
        if(args.length > 0) {
            testCaseIndex = Integer.parseInt(args[0]);
        }
        TestCaseRunner runner = new MergerParallelRunner();
        TimeLogger.getInstance().start();
        runner.run(Constants.getTestCases()[testCaseIndex]);
        TimeLogger.getInstance().stop();
    }
}
