package com.lungu;

import com.lungu.generator.TestCase;

public interface TestCaseRunner {
    void run(TestCase testCase) throws Exception;
}
