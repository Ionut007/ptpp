package com.lungu.runners;

import com.lungu.TestCaseRunner;
import com.lungu.TimeLogger;
import com.lungu.boundedBuffer.Consumer;
import com.lungu.boundedBuffer.MergerConsumer;
import com.lungu.boundedBuffer.Producer;
import com.lungu.generator.TestCase;
import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;
import com.lungu.readers.ResultWriter;
import com.lungu.readers.ScannerReader;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BarrierConsumerParallelRunner implements TestCaseRunner {
    @Override
    public void run(TestCase testCase) throws Exception {
        FullySynchronizedLinkedList linkedList = new FullySynchronizedLinkedList();
        Queue<Monom> buffer = new LinkedList<>();
        Reader reader = new ScannerReader(testCase);

        TimeLogger.getInstance().trace(testCase, "read-finished", false);

        Producer producer = new Producer(reader, buffer);

        int noOfConsumers = 256;
        List<Consumer> consumers = createConsumers(noOfConsumers, buffer);
        for(Consumer consumer : consumers) {
            consumer.start();
        }
        producer.start();

        producer.join();
        for(Consumer consumer : consumers) {
            consumer.join();
        }


        List<FullySynchronizedLinkedList> linkedLists = consumers.stream().map(Consumer::getLinkedList).collect(Collectors.toList());
        for(int mergerNumber = noOfConsumers; mergerNumber > 1; mergerNumber = mergerNumber / 2) {
            List<MergerConsumer> mergerConsumers = createMergerConsumers(linkedLists);
            for(MergerConsumer mergerConsumer : mergerConsumers) {
                mergerConsumer.start();
            }

            for(MergerConsumer mergerConsumer : mergerConsumers) {
                mergerConsumer.join();
            }
            linkedLists = mergerConsumers.stream().map(MergerConsumer::getResult).collect(Collectors.toList());
            TimeLogger.getInstance().trace(testCase, String.format("algorithm-merger-%d-starting", mergerNumber), false);
        }


        for(Consumer consumer : consumers) {
            for(Monom monom : consumer.getLinkedList()) {
                linkedList.add(monom);
            }
        }
        TimeLogger.getInstance().trace(testCase, "algorithm-finished", true);

        ResultWriter writer = new ResultWriter("par-2", testCase.getFileName());
        for(Monom monom : linkedList) {
            writer.write(monom);
        }
        writer.close();
        TimeLogger.getInstance().trace(testCase, "write-finished", false);
    }

    private List<MergerConsumer> createMergerConsumers(List<FullySynchronizedLinkedList> linkedLists) {
        List<MergerConsumer> consumers = new ArrayList<>();
        for(int i = 0; i < linkedLists.size(); i += 2) {
            consumers.add(new MergerConsumer(Arrays.asList(
                    linkedLists.get(i),
                    linkedLists.get(i + 1)
            )));
        }

        return consumers;
    }

    private List<Consumer> createConsumers(int noOfConsumers, Queue<Monom> buffer) {
        return IntStream.range(0, noOfConsumers)
                .mapToObj(i -> new Consumer(new FullySynchronizedLinkedList(), buffer))
                .collect(Collectors.toList());
    }
}
