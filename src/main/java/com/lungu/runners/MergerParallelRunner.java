package com.lungu.runners;

import com.lungu.TestCaseRunner;
import com.lungu.TimeLogger;
import com.lungu.boundedBuffer.*;
import com.lungu.generator.TestCase;
import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.list.UnSynchronizedLinkedList;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;
import com.lungu.readers.ResultWriter;
import com.lungu.readers.ScannerReader;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MergerParallelRunner implements TestCaseRunner {
    @Override
    public void run(TestCase testCase) throws Exception {
        Reader reader = new ScannerReader(testCase);
        ArrayList<Monom> buffer = new ArrayList<>();

        while(reader.hasNext()) {
            double coefficient = reader.readDouble();
            int exponent = reader.readInt();
            Monom monom = new Monom(coefficient, exponent);
            buffer.add(monom);
        }

        TimeLogger.getInstance().trace(testCase, "read-finished", false);
        int noOfConsumers = 64;

        List<MonomCreatorMerger> creators = createMergers(buffer, noOfConsumers);
        creators.forEach(Thread::start);

        for(MonomCreatorMerger creator : creators) {
            creator.join();
        }

        TimeLogger.getInstance().trace(testCase, "generated-multi-poly-data", false);
        List<UnSynchronizedLinkedList> inters = creators.stream().map(MonomCreatorMerger::getLinkedList).collect(Collectors.toList());

        List<MergerThread> mergerConsumers = null;
        for(int mergerNumber = noOfConsumers; mergerNumber > 1; mergerNumber = mergerNumber / 2) {
             mergerConsumers = createMergerConsumers(inters);
            for(MergerThread mergerConsumer : mergerConsumers) {
                mergerConsumer.start();
            }

            for(MergerThread mergerConsumer : mergerConsumers) {
                mergerConsumer.join();
            }
            inters = mergerConsumers.stream().map(MergerThread::getResult).collect(Collectors.toList());
            TimeLogger.getInstance().trace(testCase, String.format("algorithm-merger-%d-starting", mergerNumber), false);
        }

        UnSynchronizedLinkedList linkedList = mergerConsumers.get(0).getResult();
        TimeLogger.getInstance().trace(testCase, "algorithm-finished!", true);

        ResultWriter writer = new ResultWriter("par-5", testCase.getFileName());
        for(Monom monom : linkedList) {
            writer.write(monom);
        }

        writer.close();
        TimeLogger.getInstance().trace(testCase, "write-finished", false);
    }

    private List<MergerThread> createMergerConsumers(List<UnSynchronizedLinkedList> lists) {
        List<MergerThread> consumers = new ArrayList<>();
        for(int i = 0; i < lists.size(); i += 2) {
            consumers.add(new MergerThread(lists.get(i), lists.get(i + 1)));
        }

        return consumers;
    }

    private List<MonomCreatorMerger> createMergers(List<Monom> buffer, int noOfThreads) {
        List<MonomCreatorMerger> mergers = new ArrayList<>();
        int distribution = buffer.size() / noOfThreads;
        int start = 0;
        int stop = distribution;
        for(int i = 0; i < noOfThreads - 1; i++) {
            mergers.add(new MonomCreatorMerger(buffer, new UnSynchronizedLinkedList(), start, stop));
            start = stop;
            stop += distribution;
        }
        stop -= distribution;
        mergers.add(new MonomCreatorMerger(buffer, new UnSynchronizedLinkedList(), stop, buffer.size()));
        return mergers;
    }
}
