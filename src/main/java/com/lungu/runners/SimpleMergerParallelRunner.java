package com.lungu.runners;

import com.lungu.TestCaseRunner;
import com.lungu.TimeLogger;
import com.lungu.boundedBuffer.MergerConsumer;
import com.lungu.boundedBuffer.SimpleMerger;
import com.lungu.generator.TestCase;
import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;
import com.lungu.readers.ResultWriter;
import com.lungu.readers.ScannerReader;

import java.util.ArrayList;
import java.util.List;

public class SimpleMergerParallelRunner implements TestCaseRunner {
    @Override
    public void run(TestCase testCase) throws Exception {
        List<Monom> buffer = new ArrayList<>();
        Reader reader = new ScannerReader(testCase);

        while(reader.hasNext()) {
            double coefficient = reader.readDouble();
            int exponent = reader.readInt();
            buffer.add(new Monom(coefficient, exponent));
        }

        TimeLogger.getInstance().trace(testCase, "read-finished", false);
        FullySynchronizedLinkedList linkedList = new FullySynchronizedLinkedList();
        List<SimpleMerger> mergers = createMergers(buffer, 264, linkedList);

        for(SimpleMerger merger : mergers) {
            merger.start();
        }

        for(SimpleMerger merger : mergers) {
            merger.join();
        }
        TimeLogger.getInstance().trace(testCase, "algorithm-finished", true);

        ResultWriter writer = new ResultWriter("par-1", testCase.getFileName());
        for(Monom monom : linkedList) {
            writer.write(monom);
        }
        writer.close();
        TimeLogger.getInstance().trace(testCase, "write-finished", false);
    }

    private List<SimpleMerger> createMergers(List<Monom> buffer, int noOfThreads, FullySynchronizedLinkedList linkedList) {
        List<SimpleMerger> mergers = new ArrayList<>();
        int distribution = buffer.size() / noOfThreads;
        int start = 0;
        int stop = distribution;
        for(int i = 0; i < noOfThreads - 1; i++) {
            mergers.add(new SimpleMerger(buffer, linkedList, start, stop));
            start = stop;
            stop += distribution;
        }

        mergers.add(new SimpleMerger(buffer, linkedList, stop, buffer.size()));
        return mergers;
    }
}
