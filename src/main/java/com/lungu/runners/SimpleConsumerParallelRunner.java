package com.lungu.runners;

import com.lungu.TestCaseRunner;
import com.lungu.TimeLogger;
import com.lungu.boundedBuffer.Consumer;
import com.lungu.boundedBuffer.Producer;
import com.lungu.generator.TestCase;
import com.lungu.list.FullySynchronizedLinkedList;
import com.lungu.list.UnSynchronizedLinkedList;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;
import com.lungu.readers.ResultWriter;
import com.lungu.readers.ScannerReader;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class SimpleConsumerParallelRunner implements TestCaseRunner {
    @Override
    public void run(TestCase testCase) throws Exception {
        FullySynchronizedLinkedList linkedList = new FullySynchronizedLinkedList();
        Queue<Monom> buffer = new LinkedList<>();
        Reader reader = new ScannerReader(testCase);

        TimeLogger.getInstance().trace(testCase, "read-finished", false);

        Producer producer = new Producer(reader, buffer);

        List<Consumer> consumers = Arrays.asList(
            new Consumer(linkedList, buffer),
            new Consumer(linkedList, buffer),
            new Consumer(linkedList, buffer),
            new Consumer(linkedList, buffer)
        );

        for(Consumer consumer : consumers) {
            consumer.start();
        }

        producer.start();

        producer.join();

        for(Consumer consumer : consumers) {
            consumer.join();
        }


        TimeLogger.getInstance().trace(testCase, "algorithm-finished", true);

        ResultWriter writer = new ResultWriter("par-1", testCase.getFileName());
        for(Monom monom : linkedList) {
            writer.write(monom);
        }
        writer.close();
        TimeLogger.getInstance().trace(testCase, "write-finished", false);
    }
}
