package com.lungu;

import com.lungu.generator.TestCase;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TimeLogger {
    private long startTime;
    private long stopTime;
    private final static TimeLogger timeLogger = new TimeLogger();
    private List<String> logs = new ArrayList<>();

    private TimeLogger() {
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
    }

    public void stop() {
        this.stopTime = System.currentTimeMillis();
        commit();
    }

    private void commit() {
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter("analytics.txt", true));
            logs.forEach(printWriter::println);
            printWriter.flush();
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void trace(TestCase testCase, String tag, boolean commit) {
        long currentTime = System.currentTimeMillis();
        if(commit) {
            logs.add(
                    testCase.getFileName().substring(0, testCase.getFileName().length() - 1) + " " +
                            + testCase.getNoOfPolynomials() + " " + testCase.getPolynomialSize() + " " +
                            tag + " " + (currentTime - startTime));
        }
        System.out.println(testCase.getFileName() + " ---> " + "Tracing: " + tag + " " + ((currentTime - startTime) / 1000) + " s (" + (currentTime - startTime) + " ms)");
    }

    public static TimeLogger getInstance() {
        return timeLogger;
    }
}
