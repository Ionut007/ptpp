package com.lungu.list;

import java.util.Iterator;

public class LinkedIterator<T> implements Iterator<T> {
   private Node<T> head;

   public LinkedIterator(Node<T> head) {
      this.head = head;
   }

   @Override
   public boolean hasNext() {
      return head != null;
   }

   @Override
   public T next() {
      T data = head.getData();
      head = head.getNext();
      return data;
   }
}
