package com.lungu.list;

import com.lungu.polynomial.Monom;

import javax.management.remote.rmi._RMIConnection_Stub;
import java.util.Iterator;

public class UnSynchronizedLinkedList implements Iterable<Monom> {
    private long size;
    private Node<Monom> head;

    /* package private */
    public Iterator<Monom> iterator() {
        return new LinkedIterator<>(head);
    }

    public UnSynchronizedLinkedList() {
    }


    // if the power does not exist, add it
    // if the power exists, update the current value
    // if the coefficient drops to 0, delete it
    public void add(Monom data) {
        // list empty, add head
        if (head == null) {
            head = new Node<>(data);
            return;
        }
        Node<Monom> newNode = new Node<>(data);
        Node<Monom> beforePrevious = null;
        Node<Monom> previous = null;
        Node<Monom> current = head;

        // starting at the head node, crawl right before the fist greater element is and insert there
        while (current != null && current.getData().compareTo(data) <= 0) {
            beforePrevious = previous;
            previous = current;
            current = current.getNext();
        }

        // previous null, set new head
        // if previous is null, there the next monom is in no way equal to the next
        if(previous == null) {
            newNode.setNext(head);
            head = newNode;
            return;
        }

        // let's get rid of the equals case
        if(previous.getData().equals(data)) {
            previous.getData().setCoefficient(previous.getData().getCoefficient() + data.getCoefficient());
            if(previous.getData().getCoefficient() == 0) {
                size--;
                // invalid monom, delete
                if(beforePrevious != null) {
                    beforePrevious.setNext(previous.getNext());
                } else {
                    // before previous is null, so the deleted node is the head, so we replace it
                    head = previous.getNext();
                }
            }
            return;
        }

        newNode.setNext(previous.getNext());
        previous.setNext(newNode);

        size++;
    }

    public UnSynchronizedLinkedList merge(UnSynchronizedLinkedList poly) {
        Node<Monom> head1 = poly.head;
        Node<Monom> head2 = this.head;

        // start with a dummy node
        Node<Monom> dummyHead = new Node<>(null);
        Node<Monom> current = dummyHead;

        while(head1 != null && head2 != null) {
            int comp = compare(head1, head2);
            if(comp == -1) {
                current.setNext(head1);
                current = current.getNext();
                head1 = head1.getNext();
            } else if(comp == 0) {
                // equals, advance both
                double newCoef = head1.getData().getCoefficient() + head2.getData().getCoefficient();
                if(newCoef == 0) {
                    head2 = head2.getNext();
                    head1 = head1.getNext();
                    continue;
                }
                current.setNext(head1);
                current.getNext().getData().setCoefficient(head1.getData().getCoefficient() + head2.getData().getCoefficient());
                head2 = head2.getNext();
                head1 = head1.getNext();
                current = current.getNext();
            } else {
                current.setNext(head2);
                current = current.getNext();
                head2 = head2.getNext();
            }
        }

        if(head1 != null) {
            current.setNext(head1);
        } else if(head2 != null) {
            current.setNext(head2);
        } else {
            current.setNext(null);
        }

        UnSynchronizedLinkedList result = new UnSynchronizedLinkedList();
        result.head = dummyHead.getNext();

        return result;
    }

    private int compare(Node<Monom> head1, Node<Monom> head2) {
        return head1.getData().compareTo(head2.getData());
    }
}
