package com.lungu.list;

import com.lungu.polynomial.Monom;

import java.util.Iterator;

public class FullySynchronizedLinkedList implements Iterable<Monom> {
    private long size;
    private Node<Monom> head;

    /* package private */
    public Iterator<Monom> iterator() {
        return new LinkedIterator<>(head);
    }

    public FullySynchronizedLinkedList() {
    }

    public long size() {
        return size;
    }

    // if the power does not exist, add it
    // if the power exists, update the current value
    // if the coefficient drops to 0, delete it
    public synchronized void add(Monom data) {
        // list empty, add head
        if (head == null) {
            head = new Node<>(data);
            return;
        }
        Node<Monom> newNode = new Node<>(data);
        Node<Monom> beforePrevious = null;
        Node<Monom> previous = null;
        Node<Monom> current = head;

        // starting at the head node, crawl right before the fist greater element is and insert there
        while (current != null && current.getData().compareTo(data) <= 0) {
            beforePrevious = previous;
            previous = current;
            current = current.getNext();
        }

        // previous null, set new head
        // if previous is null, there the next monom is in no way equal to the next
        if(previous == null) {
            newNode.setNext(head);
            head = newNode;
            return;
        }

        // let's get rid of the equals case
        if(previous.getData().equals(data)) {
            previous.getData().setCoefficient(previous.getData().getCoefficient() + data.getCoefficient());
            if(previous.getData().getCoefficient() == 0) {
                size--;
                // invalid monom, delete
                if(beforePrevious != null) {
                    beforePrevious.setNext(previous.getNext());
                } else {
                    // before previous is null, so the deleted node is the head, so we replace it
                    head = previous.getNext();
                }
            }
            return;
        }

        newNode.setNext(previous.getNext());
        previous.setNext(newNode);

        size++;
    }
}
