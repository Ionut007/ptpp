package com.lungu;

import com.lungu.generator.TestCase;
import com.lungu.polynomial.Monom;
import com.lungu.readers.Reader;
import com.lungu.readers.ScannerReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ConsistencyRunner {
    public void run(String seqPath, String parPath) throws FileNotFoundException {
        Scanner sc1 = new Scanner(new FileInputStream(seqPath));
        Scanner sc2 = new Scanner(new FileInputStream(parPath));

        int lineNo = 0;
        while(sc1.hasNext() && sc2.hasNext()) {
            lineNo++;
            String l1 = sc1.nextLine();
            String l2 = sc2.nextLine();

            if(!l1.equals(l2)) {
                System.out.println("error at line: " + lineNo);
                System.out.println("f1 -> " + l1);
                System.out.println("f2 -> " + l2);
                return;
            }
        }
        sc1.close();
        sc2.close();
        System.out.println("Values are consistent!");

    }
}
