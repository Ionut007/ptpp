package com.lungu;

import com.lungu.runners.MergerParallelRunner;

public class ConsistencyMain {
    public static void main(String[] args) throws Exception {
//        int testCaseIndex = 7;

        for(int testCaseIndex = 0; testCaseIndex < 8; testCaseIndex++) {
            System.out.println("----- " + testCaseIndex + " ----");
            ConsistencyRunner runner = new ConsistencyRunner();
            TimeLogger.getInstance().start();
            runner.run("testcases-results/seq/test-case-" + testCaseIndex + ".txt", "testcases-results/par-5/test-case-" + testCaseIndex + ".txt");
            TimeLogger.getInstance().stop();
            System.out.println();
            System.out.println();
        }

    }
}
