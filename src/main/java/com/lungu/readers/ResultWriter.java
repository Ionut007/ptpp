package com.lungu.readers;

import com.lungu.polynomial.Monom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class ResultWriter {
    private PrintWriter printWriter;

    public ResultWriter(String type, String testCaseName) throws FileNotFoundException {
        File folder = new File("testcases-results/" + type);
        if(folder.exists()) {
            folder.delete();
        }
        folder.mkdirs();
        this.printWriter = new PrintWriter(new FileOutputStream("./testcases-results/" + type + "/" + testCaseName.substring(0, testCaseName.length() - 1) + ".txt"));
    }

    public void write(Monom monom) {
        printWriter.println(String.format("%.6f", monom.getCoefficient()) + " " + monom.getExponential());
    }

    public void close() {
        printWriter.flush();
        printWriter.close();
    }
}
