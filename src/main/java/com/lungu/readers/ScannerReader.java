package com.lungu.readers;

import com.lungu.Constants;
import com.lungu.generator.TestCase;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.Scanner;

public class ScannerReader implements Reader {
    private TestCase testCase;
    private Scanner scanner;
    private int currentIndex = -1;

    public ScannerReader(TestCase testCase) {
        this.testCase = testCase;
    }

    private void provisionScanner() throws Exception {
        if(scanner == null || !scanner.hasNext()) {
            currentIndex++;
            if(currentIndex < testCase.getNoOfPolynomials()) {
                scanner = new Scanner(new FileInputStream(getFileName(currentIndex)));
            }
        }
    }

    @Override
    public boolean hasNext() throws Exception {
        provisionScanner();
        return scanner.hasNext();
    }

    @Override
    public double readDouble() throws Exception {
        provisionScanner();

        if(!scanner.hasNext()) {
            throw new RuntimeException("Invalid read exception! End of testcase!");
        }

        return scanner.nextDouble();
    }

    @Override
    public int readInt() throws Exception {
        provisionScanner();

        if(!scanner.hasNext()) {
            throw new RuntimeException("Invalid read exception! End of testcase!");
        }

        return scanner.nextInt();
    }

    public String getFileName(int index) {
        return Constants.getPath() + testCase.getFileName() + String.format(Constants.getFileNameFormat(), index);
    }
}
