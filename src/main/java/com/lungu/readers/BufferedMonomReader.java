package com.lungu.readers;

import com.lungu.Constants;
import com.lungu.generator.TestCase;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class BufferedMonomReader implements Reader {
    private TestCase testCase;
    private BufferedReader scanner;
    private int currentIndex = -1;
    private String currentLine;

    public BufferedMonomReader(TestCase testCase) {
        this.testCase = testCase;
    }

    private void provisionScanner() throws Exception {
        if(scanner == null || currentLine == null) {
            currentIndex++;
            if(currentIndex < testCase.getNoOfPolynomials()) {
                scanner = new BufferedReader(new InputStreamReader(new FileInputStream(getFileName(currentIndex))));
                currentLine = scanner.readLine();
            }
        }
    }

    @Override
    public boolean hasNext() throws Exception {
        provisionScanner();
        return currentLine != null;
    }

    @Override
    public double readDouble() throws Exception {
        provisionScanner();

        if(currentLine == null) {
            throw new RuntimeException("Invalid read exception! End of testcase!");
        }

        return Double.parseDouble(currentLine.split(" ")[0]);
    }

    @Override
    public int readInt() throws Exception {
        provisionScanner();

        if(currentLine == null) {
            throw new RuntimeException("Invalid read exception! End of testcase!");
        }

        int val =  Integer.parseInt(currentLine.split(" ")[1]);
        currentLine = scanner.readLine();
        return val;
    }

    public String getFileName(int index) {
        return Constants.getPath() + testCase.getFileName() + String.format(Constants.getFileNameFormat(), index);
    }
}
