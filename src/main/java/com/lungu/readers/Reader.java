package com.lungu.readers;

public interface Reader {
    boolean hasNext() throws Exception;
    double readDouble() throws Exception;
    int readInt() throws Exception;
}
